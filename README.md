# DNS authoritative servers with PowerDNS

University project (Zastosowania Informatyki I) - the goal was to check out how to setup the DNS servers with PowerDNS for a domain.

## Setup 

### Primary server

```bash
# Copy .env file with environment variables for database (change them)
cp .{example.,}db.env

# Start the containers and get into the powerdns one
docker-compose up -d
docker-compose exec dns /bin/ash

# Create a zone
pdnsutil create-zone spamowisko.pl ns1.spamowisko.pl

# Add some records
pdnsutil add-record spamowisko.pl ns1 A 162.55.46.88
pdnsutil add-record spamowisko.pl ns2 A 78.47.196.155
pdnsutil add-record spamowisko.pl test A 139.177.181.243

# List records in the zone
pdnsutil list-zone spamowisko.pl
```

### Secondary server

Files for the secondary server can be found on [ns2 branch](https://gitlab.com/koralowiec/zi-powerdns/-/tree/ns2)

```bash
# Copy .env file with environment variables for database (change them)
cp .{example.,}db.env

# Start the containers and get into the powerdns one
docker-compose up -d
docker-compose exec dns /bin/ash

# Create a secondary zone called spamowisko.pl (162.55.46.88 - master's IP)
pdnsutil create-slave-zone spamowisko.pl 162.55.46.88

# Make a request for a zone transfer
pdns_control retrieve spamowisko.pl
```
